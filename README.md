## unclaimed-monies
This repo contains the solution for fixing the unclaimed monies website for PTQ.

It consists of the following:

* index.html - mock barebone website of unclaimed-monies

* ucm.js - main logic

* loading.css - loading animation

The solution uses Fetch API to send an external API request to unclaimed monies dataset in Open Data Portal QLD to replace the previous solution of using .NET MVC to retrieve the records directly from Umbraco.

A few touch-ups had been included e.g. loading animation when a request has been sent, to provide a bit of user feedback.

The rest of the changes have been made identical to reflect the current look and feel of the page to ensure that existing users can easily navigate their way to the page.