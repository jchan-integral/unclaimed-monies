const ucm = {
  init: () => {
    const searchButton = document.getElementById('searchButton');
    if (searchButton) {
      searchButton.addEventListener('click', () => ucm.getData(0, ''));
    }

    const nameInput = document.getElementById('nameTextBox');
    if (nameInput) {
      nameInput.addEventListener('keyup', (event) => {
        if (event.key === 'Enter') {
          event.preventDefault();
          searchButton.click();
        }
      });
    }

    ucm.dateFormat = (date) => {
      const p = (c) => c < 10 ? `0${c}` : c;
      const d = new Date(date);
      return [
        p(d.getDate()),
        p(d.getMonth() + 1),
        d.getFullYear()
      ].join('/');
    };

    ucm.headers();
  },

  headers: () => {
    const headers = ['ClientId_ActNo', 'Owner', 'SenderName', 'PCode', 'DateRec', 'Amount'];
    headers.forEach((header) => {
      const element = document.getElementById(header);
      element.addEventListener('click', () => ucm.sort(header, element));
    });
  },

  sort: (header, element) => {
    const sort = {
      id: header,
      type: 'desc'
    };

    const sortType = element.getAttribute('class');
    if (!sortType || sortType === 'asc') {
      element.setAttribute('class', 'desc');
    } else if (sortType === 'desc') {
      sort.type = 'asc';
      element.setAttribute('class', 'asc');
    }

    ucm.getData(0, sort);
  },

  loader: (hide = null) => {
    const loader = document.querySelector('#loading');
    if (hide) {
      loader.classList.remove('display');
    } else {
      loader.classList.add('display');
    }
  },

  buildURL: (name = '', offset, sort) => {
    const limit = 25;
    const url = 'https://www.data.qld.gov.au/api/3/action/datastore_search';
    const queryParams = new URLSearchParams({
      q: name,
      resource_id: '872065ae-ddfd-4b5f-ad15-e1935dadd883',
      limit: limit.toString(),
      ...offset > 0 && { offset },
      ...sort && { sort: `${sort.id} ${sort.type}` }
    });

    return new URL(`${url}?${queryParams}`);
  },

  getData: async (offset, sort) => {
    const tcCheckBox = document.getElementById('tcCheckBox');
    const tcError = document.querySelector('#tcError');
    const pages = document.querySelector('#pages');
    const name = document.getElementById('nameTextBox').value;
    const tableBody = document.querySelector('#tableBody');
    const errorMessage = document.querySelector('#errorMessage');

    tableBody.innerHTML = '';
    errorMessage.innerHTML = '';
    pages.innerHTML = '';

    /**
     * Do not proceed if the `T&C` checkbox is not checked.
     */
    if (!tcCheckBox.checked) {
      tcError.innerHTML = '* You must agree to the terms and conditions to proceed';
      return false;
    }

    document.getElementById('searchIntro').hidden = true;
    document.getElementById('tcDiv').hidden = true;
    document.getElementById('tcError').hidden = true;
    document.getElementById('dataTable').hidden = false;
    document.getElementById('claimSection').hidden = false;

    /**
     * Hide intro section when name has been populated
     */
    if (!name) {
      errorMessage.innerHTML = `<p>There are no results matching your search criteria: "${name}"</p>`;
      return false;
    }

    ucm.loader();
    try {
      const url = ucm.buildURL(name, parseInt(offset, 10), sort);
      let response = await fetch(url);
      response = await response.json();
      const { total, records, offset: resultOffset } = response.result;

      ucm.loader('hide');
      const rows = [];
      if (total) {
        records.forEach((record) => {
          const { ClientId_ActNo: acc, Owner, SenderName, PCode, DateRec, Amount } = record;
          const clientId = `${acc.substring(0, acc.length - 4)} / ${acc.substring(acc.length - 4, acc.length + 1)}`;
          const date = ucm.dateFormat(DateRec);
          const amount = `$${Amount}`;
          rows.push(`<tr><td class="client-ref-column">${clientId}</td><td>${Owner}</td><td>${SenderName}</td><td>${PCode}</td><td>${date}</td><td>${amount}</td></tr>`);
        });

        tableBody.innerHTML = rows.join('');

        const totalPages = Math.floor(total / 25);
        const currentOffset = resultOffset ? parseInt(resultOffset, 10) : 0;
        if (totalPages > 1) {
          ucm.generatePages(currentOffset, totalPages, sort);
        }
      } else {
        errorMessage.innerHTML = `<p>There are no results matching your search criteria: "${name.bold()}"</p>`;
      }
    } catch (error) {
      ucm.loader('hide');
      console.error(`Unable to get items: ${error}`);
      errorMessage.innerHTML = '<p>Error was encountered when retrieving the request. Please try again or at a later date.</p>';
    }
  },

  generatePages: (offset, totalPages, sort) => {
    const pages = document.querySelector('#pages');
    const maxPage = 10;
    const currentPage = offset > 0 ? offset / 25 : 0;
    let firstPage = 0;
    let lastPage = totalPages < maxPage ? totalPages : maxPage;
    let pageArrays = [];

    if (currentPage >= maxPage) {
      /**
       * This is the logic when back tracking from the total pages
       */
      if (currentPage > (totalPages - 5)) {
        firstPage = currentPage - (currentPage - (totalPages - 5));
      } else {
        firstPage = currentPage - 5;
      }

      /**
       * If the current page is more than or equal to 10, start filling the first and last page
       * in between the current page
       */
      if ((currentPage + 5) > totalPages) {
        lastPage = currentPage + (totalPages - currentPage);
      } else {
        lastPage = currentPage + 5;
      }
    }

    /**
     * "Back to the first page" option
     */
    if (currentPage >= maxPage) {
      pages.innerHTML += `<a id="start" href="#"><<</a> `;
    }

    /**
     * Dynamically assign ID's and offset value into an array for later use
     */
    for (let i = firstPage; i <= lastPage; i += 1) {
      if (i != currentPage) {
        pages.innerHTML += `<a id="a${i}" href="#">${i+1}</a> `;
        pageArrays.push({
          id: `a${i}`,
          offset: 25 * i
        });
      } else {
        pages.innerHTML += `${i+1} `;
      }
    }

    /**
     * "End of page" option
     */
    if (totalPages > maxPage && (totalPages > currentPage && currentPage < (totalPages - 5))) {
      pages.innerHTML += `<a id="end" href="#">>></a> `;
    }

    const start = document.getElementById('start');
    if (start) {
      start.addEventListener('click', () => ucm.getData(0, sort));
    }

    /**
     * Use the previous array to add event to each page
     */
    pageArrays.forEach(({ id, offset }) => {
      const pageNumber = document.getElementById(id);
      // console.log(`Added event to: ${id} with an offset of: ${offset}`);
      pageNumber.addEventListener('click', () => ucm.getData(offset, sort));
    });

    const end = document.getElementById('end');
    if (end) {
      end.addEventListener('click', () => ucm.getData(25 * totalPages, sort));
    }
  }
};

window.addEventListener('load', ucm.init);
